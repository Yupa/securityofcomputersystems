#!/bin/bash
sudo mkdir /home/u1/.ssh
sudo mkdir /home/u2/.ssh
sudo mkdir /home/u3/.ssh
sudo mkdir /root/.ssh
sudo ssh-keygen -f /home/u1/.ssh/id_rsa
sudo ssh-keygen -f /home/u2/.ssh/id_rsa
sudo ssh-keygen -f /home/u3/.ssh/id_rsa
sudo ssh-keygen -f /root/.ssh/id_rsa
sudo cp configs/u1_ssh_config /home/u1/.ssh/config
sudo cp configs/sshd_config /etc/ssh/sshd_config
