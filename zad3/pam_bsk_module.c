#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <security/pam_appl.h>
#include <security/pam_modules.h>
#include <security/pam_ext.h>

const int switch_password = 11;
const char* early_password = "early";
const char* late_password = "late";

/* expected hook, this is where custom stuff happens */
PAM_EXTERN int pam_sm_authenticate( pam_handle_t *pamh, int flags,int argc, const char **argv ) {
	int retval;

	printf("Additional authentication is required.\n");
	char* pPassword;
	retval = pam_prompt(pamh, PAM_PROMPT_ECHO_OFF, &pPassword, "Additional password: ");
	if (retval != PAM_SUCCESS) {
		return retval;
	}
	
	time_t now = time(NULL);
	struct tm early = *localtime (&now);
	early.tm_hour = switch_password;
	const char* expected_password;
	if (difftime(now, mktime(&early)) > 0.0) {
		expected_password = late_password;
	} else {
		expected_password = early_password;
	}
	if (strcmp(pPassword, expected_password) != 0) {
		printf("Wrong password. It should be `%s` instead of `%s`.\n", expected_password, pPassword);
		return PAM_AUTH_ERR;
	}
	return PAM_SUCCESS;
}