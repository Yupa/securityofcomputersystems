#include <security/pam_appl.h>
#include <security/pam_misc.h>
#include <stdio.h>
#include <string>
#include <iostream>
 
static struct pam_conv login_conv = {
  misc_conv,               /* przykładowa funkcja konwersacji z libpam_misc */
  NULL                        /* ewentualne dane aplikacji (,,domknięcie'') */
};
 
int main () {
  pam_handle_t* pamh = NULL;
  int retval;
  char *username = NULL;
 
  retval = pam_start("bsk_auth", username, &login_conv, &pamh);
  if (pamh == NULL || retval != PAM_SUCCESS) {
    fprintf(stderr, "Error when starting: %d\n", retval);
    exit(1);
  }
 
  retval = pam_acct_mgmt(pamh, 0);
  if (retval != PAM_SUCCESS) {
    fprintf(stderr, "Ten użytkownik nie ma teraz dostępu do programu.\n");
    exit(2);
  }

  pam_set_item(pamh, PAM_USER_PROMPT, "Kto to? ");
  retval = pam_authenticate(pamh, 0);  /* próba autoryzacji */
  if (retval != PAM_SUCCESS) {
    fprintf(stderr, "Autentykacja nie powiodła się.\n");
    exit(2);
  }
  fprintf(stderr, "Zalogowano.\n");

  printf("Czekam na palindromy:\n");
  while(true) {
    std::string str;
    getline(std::cin, str);
    if (str == std::string(str.rbegin(), str.rend())) {
      printf("Tak\n");
    } else {
      printf("Nie\n");
    }
    if (str.find(".") != std::string::npos) {
      break;
    }
  }
 
  printf("Koniec\n");
 
  pam_end(pamh, PAM_SUCCESS);
  exit(0);
}
