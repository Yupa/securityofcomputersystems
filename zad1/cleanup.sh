#!/bin/bash

if (($# != 3))
then
  echo "Użycie: $0 plik_z_listą_uczniów identyfikator_kolegi liczba_zadań"
  exit 1
fi

if [[ ! -f $1 ]]
then
  echo "Podany argument \"$1\" nie jest plikiem."
  exit 1
fi

if ! [[ "$3" =~ ^[0-9]+$ ]]
then
  echo "Liczba zadań musi być liczbą całkowitą"
  exit 1
fi

FILE=$1
KOLEGA=$2
LICZBA_ZADAN=$3
GRUPA="kolko_informatyczne"

# Usuniecie folderow
rm -rf documents
rm -rf tasks
rm -rf solutions

# Parsowanie pliku z lista uczniow
while IFS=" " read -r id imie nazwisko
do
  printf 'Deleting: %s\n' "$id"
  userdel "$id"
done < "$FILE"

userdel "$KOLEGA"
groupdel "$GRUPA"

exit 0