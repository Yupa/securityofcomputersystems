#!/bin/bash

if (($# != 3))
then
  echo "Użycie: $0 plik_z_listą_uczniów identyfikator_kolegi liczba_zadań"
  exit 1
fi

if [[ ! -f $1 ]]
then
  echo "Podany argument \"$1\" nie jest plikiem."
  exit 1
fi

if ! [[ "$3" =~ ^[0-9]+$ ]]
then
  echo "Liczba zadań musi być liczbą całkowitą"
  exit 1
fi

FILE=$1
KOLEGA=$2
LICZBA_ZADAN=$3
GRUPA="kolko_informatyczne"
# Dodanie grupy uczestnikow kolka
groupadd -f "$GRUPA"
# Dodanie uzytkownika kolegi
useradd "$KOLEGA"

# Stworzenie folderow
mkdir documents
mkdir tasks
mkdir solutions

# Ustawienie uprawnien do folderu `documents`.
# Kolega - czytaie i modyfikowanie plikow, ktore powstana. 
# Grupa - czytanie plikow, ktore powstana.
# Nie mozna modyfikowac struktury folderu (dodawac/usuwac plikow).
# Mozna do niego wejsc i wylistowac pliki. 
setfacl -d -m u:"$KOLEGA":rw,g:"$GRUPA":r,other::0 documents
setfacl -m u:"$KOLEGA":rx,g:"$GRUPA":rx,other::0 documents

# Ustawienie uprawnien do folderu `tasks`.
# Kolega - pelne prawa.
# Grupa - czytanie plikow, ktore powstana, dostep do folderu i ls.
setfacl -d -m u:"$KOLEGA":rwx,g:"$GRUPA":r,other::0 tasks
setfacl -m u:"$KOLEGA":rwx,g:"$GRUPA":rx,other::0 tasks

# Ustawienie uprawnien do folderu `solutions`.
# Kolega - pelne prawa.
# Wszyscy pozostali - brak praw. Do konkretnych folderow beda indywidualne prawa.
setfacl -d -m u:"$KOLEGA":rwx,other::0 solutions
setfacl -m u:"$KOLEGA":rwx,g:"$GRUPA":x,other::0 solutions

# Parsowanie pliku z lista uczniow
while IFS=" " read -r id imie nazwisko
do
  printf 'Adding user: %s\n' "$id"
  # Dodanie uzytkownika, z domyslna grupa $GRUPA.
  useradd -g "$GRUPA" "$id"
  # Dodanie folderu dla kazdego zadania i nadanie uzytkownikowi pelnych praw do tych folderow.
  for(( nr=1; nr <= "$LICZBA_ZADAN"; nr++ ))
  do
    DIR_NAME="solutions/$id-$nr"
    printf 'Creating dir: %s\n' "$DIR_NAME"
    mkdir "$DIR_NAME"
    setfacl -d -m u:"$id":rwx,u:"$KOLEGA":rwx,other::0 "$DIR_NAME"
    setfacl -m u:"$id":rwx,u:"$KOLEGA":rwx,other::0 "$DIR_NAME"
  done
done < "$FILE"

exit 0