#!/bin/bash

cd BSK/keys
sudo openvpn --dev tun --tls-client --ifconfig 10.5.0.2 10.5.0.1 --ca ca.crt --cert vpn-client.crt --key vpn-client.key --remote 172.25.1.5 --log /var/log/openvpn.log --proto tcp-client

echo 1 > /proc/sys/net/ipv4/ip_forward

iptables -A INPUT -p tcp -d 172.25.1.6 --dport 80 -j LOG --log-prefix "Dropped: " --log-level 3
iptables -A INPUT -p tcp -d 172.25.1.6 --dport 80 -j DROP

iptables -t nat -A PREROUTING -p tcp --dport 8080 -j DNAT --to-destination 10.5.0.1:80
iptables -t nat -A POSTROUTING -p tcp -d 10.5.0.1 --dport 80 -j SNAT --to-source 172.25.1.6:8080


