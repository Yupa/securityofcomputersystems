#!/bin/bash

cd BSK/keys
sudo openvpn --dev tun --tls-server --ifconfig 10.5.0.1 10.5.0.2 --ca ca.crt --cert vpn-server.crt --key vpn-server.key --dh dh2048.pem --log /var/log/openvpn.log --proto tcp-server

iptables -P INPUT DROP
iptables -A INPUT -i tun0 -j ACCEPT
iptables -A INPUT -s 10.5.0.2 -j ACCEPT
iptables -A INPUT -s 172.25.1.6 -j ACCEPT
iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

route del default
route del default gw 172.25.1.1
route add default gw 10.5.0.2

iptables-save
