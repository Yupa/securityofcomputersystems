#! /bin/sh

export DEBIAN_FRONTEND=noninteractive

apt-get update

# gdb
apt-get -y install gdb

# git
apt-get -y install git

# PEDA
git clone https://github.com/longld/peda.git ~/peda
echo "source ~/peda/peda.py" >> ~/.gdbinit

#
apt-get install cowsay
/usr/games/cowsay "Done! Moooooooo"
